# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)





Author.create(name: 'Stephen King', id: 1)
Author.create(name: 'Kant', id: 2)
Author.create(name: 'Lamer', id: 3)

Book.create(title: 'Shining', author_id: 1)
Book.create(title: 'Pets grovery', author_id: 1)
Book.create(title: 'It', author_id: 1)

Book.create(title: 'Pure mind', author_id: 2)