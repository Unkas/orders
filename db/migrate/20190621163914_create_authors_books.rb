class CreateAuthorsBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :books do |t|
      t.string :title
      t.integer :author_id
    end

    create_table :authors do |t|
      t.string :name
    end

    add_foreign_key :books, :authors, primary_key: :author_id
  end
end
